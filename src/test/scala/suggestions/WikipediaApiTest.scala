package suggestions


import gui._
import language.postfixOps
import org.junit.runner.RunWith
import org.scalatest._
import org.scalatest.junit.JUnitRunner
import rx.lang.scala._
import rx.lang.scala.subjects.{AsyncSubject, ReplaySubject}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.util.{Success, Failure}


@RunWith(classOf[JUnitRunner])
class WikipediaApiTest extends FunSuite {

  object mockApi extends WikipediaApi {
    def wikipediaSuggestion(term: String) = Future {
      if (term.head.isLetter) {
        for (suffix <- List(" (Computer Scientist)", " (Footballer)")) yield term + suffix
      } else {
        List(term)
      }
    }
    def wikipediaPage(term: String) = Future {
      "Title: " + term
    }
  }

  import mockApi._

  test("WikipediaApi should make the stream valid using sanitized") {
    val notvalid = Observable("erik", "erik meijer", "martin")
    val valid = notvalid.sanitized

    var count = 0
    var completed = false

    val sub = valid.subscribe(
      term => {
        assert(term.forall(_ != ' '))
        count += 1
      },
      t => assert(false, s"stream error $t"),
      () => completed = true
    )
    assert(completed && count == 3, "completed: " + completed + ", event count: " + count)
  }

  test("WikipediaApi should correctly use concatRecovered") {
    val requests = Observable(1, 2, 3)
    val remoteComputation = (n: Int) => Observable(0 to n)
    val responses = requests concatRecovered remoteComputation
    val sum = responses.foldLeft(0) { (acc, tn) =>
      tn match {
        case Success(n) => acc + n
        case Failure(t) => throw t
      }
    }
    var total = -1
    val sub = sum.subscribe {
      s => total = s
    }
    assert(total == (1 + 1 + 2 + 1 + 2 + 3), s"Sum: $total")
  }

  test("concatRecovered correctly composes streams with errors") {
    val subject = ReplaySubject[String]()
    subject.onNext("foo")
    val throwable = new IllegalStateException("bar")
    subject.onError(throwable)

    val result = subject.concatRecovered { string: String => Observable(s"${string}d") }
    assert(result.toBlockingObservable.toList === List(Success("food"), Failure(throwable)))

    val subject2 = ReplaySubject[String]()
    subject2.onNext("foo")
    subject2.onNext("bar")
    subject2.onNext("baz")
    subject2.onCompleted()
    val result2 = subject2.concatRecovered { string: String =>
      if (string == "bar") {
        val failureSubject = AsyncSubject[String]()
        failureSubject.onError(throwable)
        failureSubject
      } else Observable(string)
    }
    assert(result2.toBlockingObservable.toList === List(Success("foo"), Failure(throwable), Success("baz")))
  }
}